package com.driverhelp.arch;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.util.Log;

public class SomeObserver implements LifecycleObserver {
    private Owner owner;
    public static final String TAG = SomeObserver.class.getSimpleName();

    public SomeObserver(Lifecycle lifecycle, Owner owner) {
        this.owner = owner;
        lifecycle.addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void onCreate() {
        Log.d(TAG, owner + ": onCreate");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void onStop() {
        Log.d(TAG, owner + ": onStop");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    void onResume() {
        Log.d(TAG, owner + ": onResume");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    void onPause() {
        Log.d(TAG, owner + ": onPause");
    }

    public enum Owner {
        ACTIVITY, FRAGMENT, PROCESS, SERVICE
    }
}
