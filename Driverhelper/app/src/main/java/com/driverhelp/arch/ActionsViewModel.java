package com.driverhelp.arch;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.driverhelp.data.Action;
import com.driverhelp.data.database.DataBaseManager;

import java.util.List;

public class ActionsViewModel extends AndroidViewModel {

    private final LiveData<List<Action>> mActionsData;

    public ActionsViewModel(@NonNull Application application) {
        super(application);
        mActionsData = DataBaseManager.getAllActionsData();
    }

    public LiveData<List<Action>> getActionsData() {
        return mActionsData;
    }

    public void addAction(Action action) {
        DataBaseManager.addAction(action);
    }

    public void deleteAction(Action action) {
        DataBaseManager.deleteAction(action);
    }
}
