package com.driverhelp;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.crashlytics.android.Crashlytics;
import com.driverhelp.logic.Const;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import io.fabric.sdk.android.Fabric;

public class ThisApplication extends MultiDexApplication {

    private static ThisApplication mInstance;

    public static ThisApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        AppCenter.start(this, "16f89959-5d5f-4af5-9772-3acdad239d9f",
                Analytics.class, Crashes.class);

        Fabric.with(this, new Crashlytics());
        mInstance = this;
        setDayNightThemeType();
    }

    public void setDayNightThemeType() {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(this);
        String themeType = SP.getString(mInstance.getString(R.string.daynight_type), "1");
        AppCompatDelegate.setDefaultNightMode(Integer.valueOf(themeType));
    }
}
