package com.driverhelp.data.message;

import com.driverhelp.data.Action;

import java.io.Serializable;

public class MessageAction implements Serializable {

    private Action action;
    private MessageType messageType;
    private String message;

    public MessageAction(Action action, MessageType messageType, String message) {
        this.action = action;
        this.messageType = messageType;
        this.message = message;
    }

    public Action getAction() {
        return action;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public String getMessage() {
        return message;
    }
}
