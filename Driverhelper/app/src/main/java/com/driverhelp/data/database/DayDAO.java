package com.driverhelp.data.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.driverhelp.data.Day;

import java.util.List;

@Dao
public interface DayDAO {
    @Query("SELECT * FROM `day`")
    List<Day> getAll();

    @Query("SELECT * FROM `day`")
    LiveData<List<Day>> getAllData();

    @Query("SELECT * FROM `day` WHERE owner_id IS :ownerUid")
    List<Day> getDaysForWeek(long ownerUid);

    @Query("SELECT * FROM `day` WHERE owner_id IS :ownerUid AND is_ten_hours IS :isTen")
    List<Day> getTenDaysForWeek(long ownerUid, boolean isTen);

    @Query("SELECT * FROM `day` WHERE owner_id IS :ownerUid AND endTime LIKE 0")
    Day getNotEndDayForWeek(long ownerUid);

    @Query("SELECT COUNT(*) from `day`")
    int countDays();

    @Insert()
    long insert(Day day);

    @Update
    void update(Day... days);

    @Delete
    void delete(Day days);
}
