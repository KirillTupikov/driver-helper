package com.driverhelp.data;

import android.location.Location;

public class GPSData {

    private static final double SPEED_KM_H_CONSTANT = 3.6;
    private Location mlocation;
    private boolean mIsEnabled;

    public GPSData(boolean mIsEnabled) {
        this.mIsEnabled = mIsEnabled;
    }

    public GPSData(Location location) {
        this.mIsEnabled = true;
        this.mlocation = location;
    }

    public boolean isEnabled() {
        return mIsEnabled;
    }

    public String getFormattedSpeed() {
        StringBuilder sb = new StringBuilder();
        if (mlocation != null) {
            sb.append(Math.round(mlocation.getSpeed() * SPEED_KM_H_CONSTANT));
        }
        sb.append(" км/ч");
        return sb.toString();
    }
}
