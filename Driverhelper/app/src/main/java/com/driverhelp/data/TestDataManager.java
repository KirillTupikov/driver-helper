package com.driverhelp.data;

import android.text.format.DateUtils;

import java.util.ArrayList;

public class TestDataManager {

    private static TestDataManager mInstance;

    public static TestDataManager getInstance() {
        if (mInstance == null) {
            mInstance = new TestDataManager();
        }

        return mInstance;
    }

    private ArrayList<Action> mActions;

    private TestDataManager() {
        initEvents();
    }

    public ArrayList<Action> getEvents() {
        return mActions;
    }

    private void initEvents() {
        mActions = new ArrayList<>();

        Action action = new Action(ActionType.DRIVE, System.currentTimeMillis() - 3 * DateUtils.HOUR_IN_MILLIS);
        mActions.add(action);

        action = new Action(ActionType.REST, System.currentTimeMillis() - 15 * DateUtils.MINUTE_IN_MILLIS);
        mActions.add(action);

        action = new Action(ActionType.DRIVE, System.currentTimeMillis() - 3 * DateUtils.HOUR_IN_MILLIS);
        mActions.add(action);

        action = new Action(ActionType.REST, System.currentTimeMillis() - 45 * DateUtils.MINUTE_IN_MILLIS);
        mActions.add(action);

        action = new Action(ActionType.POA, System.currentTimeMillis() - 2 * DateUtils.HOUR_IN_MILLIS);
        mActions.add(action);

        action = new Action(ActionType.WORK, System.currentTimeMillis() - 45 * DateUtils.MINUTE_IN_MILLIS);
        mActions.add(action);
    }
}
