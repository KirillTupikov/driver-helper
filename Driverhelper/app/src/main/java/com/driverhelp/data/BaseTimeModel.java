package com.driverhelp.data;

import android.arch.persistence.room.ColumnInfo;

public abstract class BaseTimeModel extends BaseDataModel {

    public static final String START_TIME = "startTime";
    public static final String END_TIME = "endTime";

    @ColumnInfo(name = START_TIME)
    protected long startTime;
    @ColumnInfo(name = END_TIME)
    protected long endTime;

    public BaseTimeModel() {
    }

    public BaseTimeModel(long startTime) {
        this.startTime = startTime;
    }

    public BaseTimeModel(long startTime, long endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getRealTimeDuration() {
        return endTime == 0 ? System.currentTimeMillis() - startTime : (endTime - startTime);
    }
}
