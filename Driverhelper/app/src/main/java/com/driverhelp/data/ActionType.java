package com.driverhelp.data;

import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.driverhelp.R;

public enum ActionType {

    DRIVE(R.string.event_drive, R.drawable.ic_drive, R.drawable.ic_drive_min, R.color.orange_drive),
    WORK(R.string.event_shipment, R.drawable.ic_work, R.drawable.ic_work_min, R.color.yellow_work),
    POA(R.string.event_idling, R.drawable.ic_poa, R.drawable.ic_poa_min, R.color.ligt_blue_poa),
    REST(R.string.event_rest, R.drawable.ic_rest, R.drawable.ic_rest_min, R.color.dark_blue_rest),
    EMPTY(0, 0, 0, 0);

    private int titleRes;
    private int iconRes;
    private int iconMinRes;
    private int iconColor;

    ActionType(@StringRes int titleRes, @DrawableRes int iconRes,
               @DrawableRes int iconMinRes, @ColorRes int iconColor) {
        this.titleRes = titleRes;
        this.iconRes = iconRes;
        this.iconMinRes = iconMinRes;
        this.iconColor = iconColor;
    }

    @StringRes
    public int getTitleRes() {
        return titleRes;
    }

    @ColorRes
    public int getIconColor() {
        return iconColor;
    }

    @DrawableRes
    public int getIconRes() {
        return iconRes;
    }

    @DrawableRes
    public int getIconMinRes() {
        return iconMinRes;
    }
}
