package com.driverhelp.data.message;

public class MessageTime {

    private long calcDuration;
    private MessageType type;
    private String message;

    public MessageTime(long calcDuration, MessageType type, String message) {
        this.calcDuration = calcDuration;
        this.type = type;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public long getCalcDuration() {
        return calcDuration;
    }

    public MessageType getType() {
        return type;
    }
}
