package com.driverhelp.data.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.driverhelp.data.WorkSession;

import java.util.List;

@Dao
public interface WorkSessionDAO {
    @Query("SELECT * FROM `worksession`")
    List<WorkSession> getAll();

    @Query("SELECT * FROM `worksession`")
    LiveData<List<WorkSession>> getAllData();

    @Query("SELECT COUNT(*) from `worksession`")
    int countWorkSessions();

    @Query("SELECT * FROM `worksession` WHERE endTime LIKE 0")
    List<WorkSession> findNotEndSessions();

    @Insert()
    long insert(WorkSession workSession);

    @Update
    void update(WorkSession... workSessions);

    @Delete
    void delete(WorkSession workSessions);
}
