package com.driverhelp.data.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.driverhelp.data.Action;

import java.util.List;

@Dao
public interface ActionDAO {

    @Query("SELECT * FROM `action`")
    List<Action> getAll();

    @Query("SELECT * FROM `action`")
    LiveData<List<Action>> getAllData();

    @Query("SELECT * FROM `action` where type LIKE  :typeId")
    List<Action> findByType(int typeId);

    @Query("SELECT * FROM `action` where type LIKE  :typeId AND owner_id IS :ownerUid")
    List<Action> findByTypeForDay(int typeId, long ownerUid);

    @Query("SELECT * FROM `action` where owner_id LIKE :ownerUid AND type LIKE :driveTypeId OR owner_id LIKE :ownerUid AND type LIKE :restTypeId")
    List<Action> findDriveRelaxForDay(int driveTypeId, int restTypeId, long ownerUid);

    @Query("SELECT * FROM `action` WHERE owner_id IS :ownerUid")
    List<Action> getActionsForDay(long ownerUid);

    @Query("SELECT * FROM `action` WHERE owner_id IS :ownerUid AND endTime LIKE 0")
    Action getNotEndActionForDay(long ownerUid);

    @Query("SELECT COUNT(*) from `action`")
    int countActions();

    @Insert()
    long insert(Action action);

    @Update
    void update(Action... action);

    @Delete
    void delete(Action action);
}
