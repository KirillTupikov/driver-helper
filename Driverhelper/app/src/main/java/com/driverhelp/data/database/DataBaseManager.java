package com.driverhelp.data.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import com.driverhelp.ThisApplication;
import com.driverhelp.data.Action;
import com.driverhelp.data.ActionType;
import com.driverhelp.data.Day;
import com.driverhelp.data.Week;
import com.driverhelp.data.WorkSession;

import java.util.List;

import static com.driverhelp.logic.Const.DAY_SESSION_MAX;

@Database(entities = {Action.class, Day.class, Week.class, WorkSession.class}, version = 1)
public abstract class DataBaseManager extends RoomDatabase {

    private static DataBaseManager mInstance;

    public abstract ActionDAO actionDao();

    public abstract DayDAO dayDAO();

    public abstract WeekDAO weekDAO();

    public abstract WorkSessionDAO workSessionDAO();

    private static DataBaseManager getDataBase() {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(ThisApplication.getInstance(), DataBaseManager.class, "driver_helper_database")
                    .allowMainThreadQueries()
                    .build();
        }
        return mInstance;
    }

    public static void destroyInstance() {
        mInstance = null;
    }

    //Actions
    public static long addAction(Action action) {
        return getDataBase().actionDao().insert(action);
    }

    public static void updateAction(Action action) {
        getDataBase().actionDao().update(action);
    }

    public static List<Action> getAllActions() {
        return getDataBase().actionDao().getAll();
    }

    public static LiveData<List<Action>> getAllActionsData() {
        return getDataBase().actionDao().getAllData();
    }

    public static List<Action> getActionsForDay(long ownerUid) {
        return getDataBase().actionDao().getActionsForDay(ownerUid);
    }

    public static List<Action> getActionsByType(ActionType actionType) {
        return getDataBase().actionDao().findByType(actionType.ordinal());
    }

    public static List<Action> getActionsByTypeForDay(ActionType actionType, long ownerUid) {
        return getDataBase().actionDao().findByTypeForDay(actionType.ordinal(), ownerUid);
    }

    public static List<Action> getActionsDriveRelaxForDay(long ownerUid) {
        return getDataBase().actionDao().findDriveRelaxForDay(ActionType.DRIVE.ordinal(), ActionType.REST.ordinal(), ownerUid);
    }

    public static Action getNotEndActionForDay(long ownerUid) {
        return getDataBase().actionDao().getNotEndActionForDay(ownerUid);
    }

    public static void deleteAction(Action action) {
        getDataBase().actionDao().delete(action);
    }

    //Days

    public static void updateDay(Day day) {
        getDataBase().dayDAO().update(day);
    }

    public static long addDay(Day day) {
        return getDataBase().dayDAO().insert(day);
    }

    public static List<Day> getAllDays() {
        return getDataBase().dayDAO().getAll();
    }

    public static List<Day> getDaysForWeek(long ownerUid) {
        return getDataBase().dayDAO().getDaysForWeek(ownerUid);
    }

    public static List<Day> getTenDaysForWeek(long ownerUid, boolean isTen) {
        return getDataBase().dayDAO().getTenDaysForWeek(ownerUid, isTen);
    }

    public static Day getNotEndDayForWeek(long ownerUid) {
        return getDataBase().dayDAO().getNotEndDayForWeek(ownerUid);
    }

    //Weeks
    public static long addWeek(Week week) {
        return getDataBase().weekDAO().insert(week);
    }

    public static List<Week> getAllWeeks() {
        return getDataBase().weekDAO().getAll();
    }

    public static List<Week> getWeeksForWorkSession(long ownerUid) {
        return getDataBase().weekDAO().getWeekForWorkSession(ownerUid);
    }

    public static Week getNotEndWeekForWorkSession(long ownerUid) {
        return getDataBase().weekDAO().getNotEndWeekForWorkSession(ownerUid);
    }

    //WorkSessions
    public static long addWorkSession(WorkSession workSession) {
        return getDataBase().workSessionDAO().insert(workSession);
    }

    public static List<WorkSession> getAllWorkSessions() {
        return getDataBase().workSessionDAO().getAll();
    }

    public static List<WorkSession> getNotEndSession() {
        return getDataBase().workSessionDAO().findNotEndSessions();
    }

    public static void updateWorkSession(WorkSession session) {
        getDataBase().workSessionDAO().update(session);
    }

    public static long getRealDayRemainingTime(long ownerUid) {
        List<Action> allActions = getDataBase().actionDao().getActionsForDay(ownerUid);
        long allDataDurationPerDay = 0;
        for (Action a : allActions) {
            allDataDurationPerDay += a.getActualDuration();
        }
        return DAY_SESSION_MAX - allDataDurationPerDay;
    }
}
