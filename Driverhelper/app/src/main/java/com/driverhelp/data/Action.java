package com.driverhelp.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.text.format.DateUtils;

import java.util.Random;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "action",
        foreignKeys = @ForeignKey(
                entity = Day.class,
                parentColumns = "uid",
                childColumns = "owner_id",
                onDelete = CASCADE))
public class Action extends BaseTimeModel {

    public static final String TYPE = "type";
    public static final String OWNER_ID = "owner_id";
    public static final String IS_ERROR = "is_error";
    public static final String ACTUAL_DURATION = "duration";
    public static final String REST_TYPE = "rest_type";

    @ColumnInfo(name = TYPE)
    private int type;
    @ColumnInfo(name = OWNER_ID)
    private long ownerUid;
    @ColumnInfo(name = IS_ERROR)
    private boolean isError;
    @ColumnInfo(name = ACTUAL_DURATION)
    private long actualDuration;
    @ColumnInfo(name = REST_TYPE)
    private int restType;

    @Deprecated
    public Action(ActionType type, long startTime) {
        Random random = new Random();
        this.type = type.ordinal();
        this.startTime = startTime;
        this.endTime = startTime + DateUtils.MINUTE_IN_MILLIS * random.nextInt(50);
    }

    public Action(long startTime, ActionType type, long ownerUid) {
        super(startTime);
        this.type = type.ordinal();
        this.ownerUid = ownerUid;
    }

    public Action(ActionType type, long startTime, long endTime) {
        this.type = type.ordinal();
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Action(int type) {
        this.type = type;
    }

    public ActionType getActionType() {
        return ActionType.values()[type];
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getOwnerUid() {
        return ownerUid;
    }

    public void setOwnerUid(long ownerUid) {
        this.ownerUid = ownerUid;
    }

    @Override
    public String toString() {
        return getActionType().name();
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public int getRestType() {
        return restType;
    }

    public void setRestType(int restType) {
        this.restType = restType;
    }

    public RestType getRestTypeEnum() {
        return RestType.values()[restType];
    }

    public void setRestTypeEnum(RestType restType) {
        this.restType = restType.ordinal();
    }

    public long getActualDuration() {
        return actualDuration;
    }

    //Фактическое значение ( Отдохнул полтора часа, засчитало 45 мин)
    public void setActualDuration(long actualDuration) {
        this.actualDuration = actualDuration;
    }
}
