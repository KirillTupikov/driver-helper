package com.driverhelp.data;

/**
 * Created by Sant on 25.11.2017.
 */

public class ActivityType {

    private String activityName;

    public ActivityType(String string) {
        this.activityName = string;
    }

    public String getActivityName() {
        return activityName;
    }
}
