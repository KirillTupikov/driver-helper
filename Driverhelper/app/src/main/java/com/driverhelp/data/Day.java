package com.driverhelp.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "day",
        foreignKeys = @ForeignKey(
                entity = Week.class,
                parentColumns = "uid",
                childColumns = "owner_id",
                onDelete = CASCADE))
public class Day extends BaseTimeModel {

    public static final String OWNER_ID = "owner_id";
    public static final String TEN_HOURS = "is_ten_hours";

    @ColumnInfo(name = OWNER_ID)
    private long ownerUid;

    @ColumnInfo(name = TEN_HOURS)
    public boolean isTenHours;

    @Ignore
    public long sumDrive;
    @Ignore
    public long sumPoa;
    @Ignore
    public long sumWork;
    @Ignore
    public long sumRelax;
    @Ignore
    public long sumRealRelax;
    @Ignore
    public long sumAll;

    public Day(long startTime, long ownerUid) {
        super(startTime);
        this.isTenHours = false;
        this.ownerUid = ownerUid;
    }

    public long getOwnerUid() {
        return ownerUid;
    }

    public void setOwnerUid(long ownerUid) {
        this.ownerUid = ownerUid;
    }

    public boolean isTenHours() {
        return isTenHours;
    }

    public void setTenHours(boolean isTen) {
        this.isTenHours = isTen;
    }
}
