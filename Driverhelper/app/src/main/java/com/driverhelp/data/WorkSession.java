package com.driverhelp.data;

import android.arch.persistence.room.Entity;

@Entity(tableName = "worksession")
public class WorkSession extends BaseTimeModel {

    public WorkSession(long startTime) {
        this.startTime = startTime;
    }
}
