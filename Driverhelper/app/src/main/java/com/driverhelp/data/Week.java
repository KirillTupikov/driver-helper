package com.driverhelp.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "week",
        foreignKeys = @ForeignKey(
                entity = WorkSession.class,
                parentColumns = "uid",
                childColumns = "owner_id",
                onDelete = CASCADE))
public class Week extends BaseTimeModel {

    public static final String OWNER_ID = "owner_id";

    @ColumnInfo(name = OWNER_ID)
    private long ownerUid;

    public Week(long startTime, long ownerUid) {
        super(startTime);
        this.ownerUid = ownerUid;
    }

    public long getOwnerUid() {
        return ownerUid;
    }

    public void setOwnerUid(long ownerUid) {
        this.ownerUid = ownerUid;
    }
}
