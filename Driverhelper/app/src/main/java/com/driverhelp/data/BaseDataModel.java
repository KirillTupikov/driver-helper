package com.driverhelp.data;

import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

public abstract class BaseDataModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    protected long uid;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
