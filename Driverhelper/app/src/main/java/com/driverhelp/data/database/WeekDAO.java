package com.driverhelp.data.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.driverhelp.data.Week;

import java.util.List;

@Dao
public interface WeekDAO {
    @Query("SELECT * FROM `week`")
    List<Week> getAll();

    @Query("SELECT * FROM `week`")
    LiveData<List<Week>> getAllData();

    @Query("SELECT * FROM `week` WHERE owner_id IS :ownerUid")
    List<Week> getWeekForWorkSession(long ownerUid);

    @Query("SELECT * FROM `week` WHERE owner_id IS :ownerUid AND endTime LIKE 0")
    Week getNotEndWeekForWorkSession(long ownerUid);

    @Query("SELECT COUNT(*) from `week`")
    int countWeeks();

    @Insert()
    long insert(Week week);

    @Update
    void update(Week... weeks);

    @Delete
    void delete(Week weeks);
}
