package com.driverhelp.widgets;

import android.support.v4.app.Fragment;

public interface FragmentPagerInterface {

    Fragment getFragment(int position);

    int getCount();

    float getPageWidth(int position);
}
