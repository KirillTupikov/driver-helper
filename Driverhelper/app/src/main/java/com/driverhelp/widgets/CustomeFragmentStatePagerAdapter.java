package com.driverhelp.widgets;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

public abstract class CustomeFragmentStatePagerAdapter extends FragmentStatePagerAdapter implements FragmentPagerInterface {

    public CustomeFragmentStatePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object obj = super.instantiateItem(container, position);
        if (obj instanceof Fragment) {
            Fragment fragment = (Fragment) obj;
            setFragmentForPosition(fragment, position);
            return fragment;
        } else {
            return null;
        }
    }

    protected SparseArray<Fragment> mFragments = new SparseArray<Fragment>();

    protected void setFragmentForPosition(Fragment fragment, int position) {
        mFragments.put(position, fragment);
    }

    public Fragment getFragment(int position) {
        if (mFragments != null) {
            return mFragments.get(position);
        } else {
            return null;
        }
    }
}
