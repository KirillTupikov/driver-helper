package com.driverhelp.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class NonSwipeableViewPager extends ViewPager {

    private boolean isCanScroll = true;

    public NonSwipeableViewPager(Context context) {
        super(context);
    }

    public NonSwipeableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if(isCanScroll) {
           return super.onInterceptTouchEvent(event);
        }

        // Never allow swiping to switch between pages
        return false;
    }

    public void setCanScroll(boolean isCanScroll) {
        this.isCanScroll = isCanScroll;
    }

    public interface OnCanScrollChangeListener {
        void onChangeScroll(boolean isCanScroll);
    }
}
