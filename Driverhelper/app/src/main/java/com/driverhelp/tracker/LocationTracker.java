package com.driverhelp.tracker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import com.driverhelp.utils.EventBusHelper;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import org.greenrobot.eventbus.EventBus;

import static com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY;


public class LocationTracker implements LocationListener{

    private static final int UPDATE_LOCATION_INTERVAL = 10000;
    private static final int FAST_UPDATE_LOCATION_INTERVAL = 5000;
    private static LocationTracker instance = null;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;

    public static LocationTracker getLocationTracker(Context context) {
        if (instance == null) {
            instance = new LocationTracker(context);
        }
        return instance;
    }

    private LocationTracker(Context context) {
        initLocationService(context);
        enableLocationTracking();
    }

    private void initLocationService(Context context) {
        mFusedLocationClient = new FusedLocationProviderClient(context);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                final Location lastlocation = locationResult.getLastLocation();
                EventBus.getDefault().post(new EventBusHelper.GPSEvent(lastlocation));
            }
        };
    }

    public static boolean validateGPSProvider(Context context) {
        LocationManager locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        boolean isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        EventBus.getDefault().post(new EventBusHelper.GPSEvent(isGPSEnable));
        return isGPSEnable;
    }

    @SuppressLint("MissingPermission")
    private void enableLocationTracking() {

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(FAST_UPDATE_LOCATION_INTERVAL);
        locationRequest.setInterval(UPDATE_LOCATION_INTERVAL);

        mFusedLocationClient.requestLocationUpdates(locationRequest,
                mLocationCallback,
                null);
    }

    @Override
    public void onLocationChanged(Location location) {
        EventBus.getDefault().post(new EventBusHelper.GPSEvent(location));
    }
}
