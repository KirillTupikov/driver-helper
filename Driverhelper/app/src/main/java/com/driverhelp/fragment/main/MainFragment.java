package com.driverhelp.fragment.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.CycleInterpolator;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import com.driverhelp.R;
import com.driverhelp.ThisApplication;
import com.driverhelp.data.Action;
import com.driverhelp.data.ActionType;
import com.driverhelp.data.RestType;
import com.driverhelp.data.message.MessageAction;
import com.driverhelp.fragment.BaseActionsFragment;
import com.driverhelp.logic.LogicManager;
import com.driverhelp.utils.DateUtils;
import com.driverhelp.utils.DialogUtils;
import com.driverhelp.utils.EventBusHelper;
import com.driverhelp.utils.MessageUtils;
import com.driverhelp.widgets.CheckableImageButton;

import org.greenrobot.eventbus.Subscribe;

import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.driverhelp.data.ActionType.DRIVE;
import static com.driverhelp.data.ActionType.POA;
import static com.driverhelp.data.ActionType.REST;
import static com.driverhelp.data.ActionType.WORK;
import static com.driverhelp.logic.Const.TEST_COF;
import static com.driverhelp.logic.Const.TIMER_INTERVAL_MS;

public class MainFragment extends BaseActionsFragment implements CheckableImageButton.OnCheckedChangeListener, LogicManager.UpdateTimerListener {

    public static final String TIME_FORMAT = "00:%s";
    private CheckableImageButton mDriverButton;
    private CheckableImageButton mWorkButton;
    private CheckableImageButton mPoaButton;
    private CheckableImageButton mRestButton;
    private CheckableImageButton mCheckedButton;

    private View mAnimatableBackground;
    private View mDriveView;
    private View mDefaultView;
    private TextView mRelaxText;
    private ImageView mMainIcon;
    private TextView mActionText;
    private TextView mIntroText;
    private TextView mSpeedText;
    private TextView mActivityText;
    private TextView mWarningText;
    private TextView mDriveRemainingTime;
    private Chronometer mChronometer;
    private Chronometer mDriveChronometer;
    private CountDownTimer mDriveCountDownTimer;
    private Button mStart;
    private MediaPlayer mp;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LogicManager.getInstance().setUpdateTimerListener(this);
        initUi(view);
        setListeners();
        long time = SystemClock.elapsedRealtime();
        Chronometer.OnChronometerTickListener chrListener = chronometer -> {
            long tikTime = (SystemClock.elapsedRealtime() - chronometer.getBase()) * TEST_COF;
            chronometer.setText(DateUtils.getDurationTime(tikTime));
        };
        mChronometer.setBase(time);
        mChronometer.setFormat(TIME_FORMAT);
        mChronometer.setOnChronometerTickListener(chrListener);
        mDriveChronometer.setBase(time);
        mDriveChronometer.setFormat(TIME_FORMAT);
        mDriveChronometer.setOnChronometerTickListener(chrListener);
        mStart.setOnClickListener(view1 -> {
            mIntroText.setVisibility(VISIBLE);
            mIntroText.setText(R.string.intro_text2);
            mStart.setVisibility(INVISIBLE);
            mDriverButton.setVisibility(VISIBLE);
            mWorkButton.setVisibility(VISIBLE);
            mPoaButton.setVisibility(VISIBLE);
            mRestButton.setVisibility(VISIBLE);
        });
        if (LogicManager.getInstance().isWorkSessionActive()) {
            mDriveView.setVisibility(VISIBLE);
            mDefaultView.setVisibility(GONE);
            mIntroText.setVisibility(GONE);
            mStart.setVisibility(GONE);
            mDriverButton.setVisibility(VISIBLE);
            mWorkButton.setVisibility(VISIBLE);
            mPoaButton.setVisibility(VISIBLE);
            mRestButton.setVisibility(VISIBLE);
            LogicManager.getInstance().setUpdateTimerListener(this);
//            long duration = System.currentTimeMillis() - LogicManager.getInstance().getLastAction().getStartTime();
            Action action = LogicManager.getInstance().getLastAction();
            switch (action.getActionType()) {
                case DRIVE:
                    mDriverButton.setChecked(true);
                    break;

                case WORK:
                    mChronometer.start();
                    mWorkButton.setChecked(true);
                    break;

                case POA:
                    mChronometer.start();
                    mPoaButton.setChecked(true);
                    break;

                case REST:
                    mChronometer.start();
                    mRestButton.setChecked(true);
                    break;
            }
        }
    }

    private void initUi(@NonNull View view) {
        mDriveView = view.findViewById(R.id.drive_view);
        mDefaultView = view.findViewById(R.id.default_view);
        mActivityText = view.findViewById(R.id.activity_text);
        mDriverButton = view.findViewById(R.id.btn_drive);
        mWorkButton = view.findViewById(R.id.btn_work);
        mPoaButton = view.findViewById(R.id.btn_poa);
        mRestButton = view.findViewById(R.id.btn_rest);
        mChronometer = view.findViewById(R.id.chronometer);
        mDriveChronometer = view.findViewById(R.id.drive_chronometer);
        mRelaxText = view.findViewById(R.id.relax_text);
        mMainIcon = view.findViewById(R.id.main_icon);
        mActionText = view.findViewById(R.id.action_text);
        mIntroText = view.findViewById(R.id.intro_text);
        mStart = view.findViewById(R.id.start_btn);
        mSpeedText = view.findViewById(R.id.speed_text);
        mWarningText = view.findViewById(R.id.warning_text);
        mDriveRemainingTime = view.findViewById(R.id.drive_remaining_time);
        mAnimatableBackground = view.findViewById(R.id.animatable_background);
    }

    private void setListeners() {
        mWorkButton.setOnCheckedChangeListener(this);
        mDriverButton.setOnCheckedChangeListener(this);
        mPoaButton.setOnCheckedChangeListener(this);
        mRestButton.setOnCheckedChangeListener(this);
    }

    @Override
    protected void handleMessageFromService(MessageAction messageAction) {
        switch (messageAction.getMessageType()) {
            case MESSAGE:
                showWarningMessage(messageAction.getMessage());
                break;

            case DIALOG:
                switch (messageAction.getAction().getActionType()) {
                    case POA:
                        MessageUtils.showMessage(getActivity(), messageAction, () -> {
                            mRestButton.setChecked(true);
                        });
                        break;

                    default:
                        MessageUtils.showMessage(getActivity(), messageAction, () -> {
                        });
                }
        }
    }

    @Override
    protected void onNewEvent(Action action) {
    }

    @Override
    public void onCheckedChanged(CheckableImageButton button, boolean isChecked) {

        mIntroText.setVisibility(GONE);

        if (mCheckedButton != null && mCheckedButton != button) {
            mCheckedButton.setChecked(false);
        }

        mCheckedButton = button;
        mChronometer.stop();
        mDriveChronometer.stop();
        mWarningText.setText("");

        ActionType actionType = null;
        if (isChecked) {
            switch (button.getId()) {
                case R.id.btn_drive:
                    actionType = DRIVE;
                    break;
                case R.id.btn_work:
                    actionType = WORK;
                    mRelaxText.setVisibility(INVISIBLE);
                    break;
                case R.id.btn_poa:
                    actionType = POA;
                    mRelaxText.setVisibility(INVISIBLE);
                    break;
                case R.id.btn_rest:
                    if (!LogicManager.getInstance().isDriveTimeEnd()) {

                        DialogUtils.showRestTypeDialog(getActivity(), (id) -> {
                            RestType restType = null;
                            switch (id) {
                                case R.id.start_break:
                                    restType = RestType.SHORT;
                                    mRelaxText.setText(getString(R.string.start_break));
                                    break;

                                case R.id.start_daily:
                                    restType = RestType.DAY;
                                    mRelaxText.setText(getString(R.string.start_daily_rest));
                                    break;

                                case R.id.start_weekly:
                                    restType = RestType.WEAK;
                                    mRelaxText.setText(getString(R.string.start_weekly_rest));
                                    break;
                            }

                            mRelaxText.setVisibility(VISIBLE);
                            setAction(REST, restType);
                        }, () -> {
                            button.setChecked(false);
                        });

                        clearItems();
                    } else {
                        mRelaxText.setText(getString(R.string.start_daily_rest));
                        mRelaxText.setVisibility(VISIBLE);
                        setAction(REST, RestType.DAY);
                    }
                    return;
            }
        }

        if (actionType != null) {
            setAction(actionType, null);
        } else {
            clearItems();
        }
    }

    private void setAction(ActionType actionType, @Nullable RestType restType) {
        setTextTheme(actionType.getIconColor(), actionType.getIconMinRes());
        LogicManager.getInstance().addNewAction(actionType, restType, new LogicManager.PostNewActionListener() {
            @Override
            public void createdDay(boolean isTenAnswer) {
                if (isTenAnswer) {
                    DialogUtils.showWorkTypeDialog(getActivity(), (id) -> {
                        switch (id) {
                            case R.id.nine_hours:
                                LogicManager.getInstance().setWorkDayType(false);
                                break;

                            case R.id.ten_hours:
                                LogicManager.getInstance().setWorkDayType(true);
                                break;

                            default:
                                break;
                        }
                    }, getString(R.string.dialog_chose_work_type), null);
                }
            }

            @Override
            public void createdAction(Action action) {

            }
        });

        mActionText.setText(actionType.getTitleRes());
        mActionText.setVisibility(VISIBLE);
        if (actionType == DRIVE) {
            mDriveView.setVisibility(VISIBLE);
            mDefaultView.setVisibility(GONE);
            mDriveChronometer.start();
            mDriveChronometer.setBase(SystemClock.elapsedRealtime());
        } else {
            mDriveView.setVisibility(GONE);
            mDefaultView.setVisibility(VISIBLE);
            mChronometer.setVisibility(VISIBLE);
            mChronometer.start();
            mChronometer.setBase(SystemClock.elapsedRealtime());
        }
    }

    private void clearItems() {
        mRelaxText.setVisibility(INVISIBLE);
        mActionText.setVisibility(GONE);
        mChronometer.setVisibility(GONE);
        mDriveView.setVisibility(GONE);
        mDefaultView.setVisibility(GONE);
        setTextTheme(R.color.colorAccent, -1);
        mChronometer.stop();
        mDriveChronometer.stop();
    }

    private void setTextTheme(@ColorRes int colorRes, @DrawableRes int drawableRes) {
        Context context = getContext();
        if (context != null) {
            final int textColor = ContextCompat.getColor(context, colorRes);

            mActionText.setTextColor(textColor);
            mChronometer.setTextColor(textColor);

            if (drawableRes == -1) {
                mMainIcon.setImageDrawable(null);
            } else {
                mMainIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), drawableRes));
            }
        }
    }

    @Subscribe
    public void onEvent(EventBusHelper.GPSEvent gpsEvent) {
        if (gpsEvent != null) {
            mSpeedText.setText(gpsEvent.getData().getFormattedSpeed());
        }
    }

    @Subscribe
    public void onEvent(EventBusHelper.ActivityTypeEvent activityTypeEvent) {
        if (activityTypeEvent != null) {
            mActivityText.post(() -> mActivityText.setText(activityTypeEvent.getData()));
        }
    }

    @Subscribe
    public void onEvent(EventBusHelper.EndSessionEvent endSessionEvent) {
        if (endSessionEvent != null) {
            clearItems();
            if (mCheckedButton != null) {
                mCheckedButton.setChecked(false);
            }
            mIntroText.setText(R.string.intro_text);
            mIntroText.setVisibility(VISIBLE);
            mStart.setVisibility(VISIBLE);
            mDriverButton.setVisibility(INVISIBLE);
            mWorkButton.setVisibility(INVISIBLE);
            mPoaButton.setVisibility(INVISIBLE);
            mRestButton.setVisibility(INVISIBLE);
            if (!endSessionEvent.isService()) {
                Log.d("TAG", "service stopped");
                LogicManager.getInstance().stopWorkSession();
            }
        }
    }

    private boolean isAnimated = false;

    private void stopAnimate() {
        isAnimated = false;
        mWarningText.animate().cancel();
    }

    private void startWarningAnimation() {
        if (!isAnimated) {
            mWarningText.animate()
                    .alpha(0f)
                    .setDuration(14000)
                    .setInterpolator(new CycleInterpolator(5))
                    .setListener(new AnimatorListenerAdapter() {

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            isAnimated = false;
                        }
                    })
                    .start();
            isAnimated = true;
        }
    }

    private void setDriveCountDonwnTimer(long millsInFuture) {
        if (mDriveCountDownTimer != null) {
            mDriveCountDownTimer.cancel();
        }
        if (millsInFuture < 0) {
            sendDriveViolation();
            return;
        }
        mDriveCountDownTimer = new CountDownTimer(millsInFuture, (int) TIMER_INTERVAL_MS) {
            public void onTick(long millisUntilFinished) {
                if (isDriveActionCurret()) {
                    setTimerString(formatTimeString(millisUntilFinished));
                }
            }

            public void onFinish() {
                sendDriveViolation();
            }
        }.start();
    }

    @Override
    public void onDetach() {
        if (mDriveCountDownTimer != null) {
            mDriveCountDownTimer.cancel();
            mDriveCountDownTimer = null;
            mDriveRemainingTime = null;
        }
        LogicManager.getInstance().clearUpdateTimerListener();
        super.onDetach();
    }

    public void showErrorAlert() {
        int colorFrom = getResources().getColor(R.color.whiteBlack);
        int colorTo = Color.RED;
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo, colorFrom);
        colorAnimation.setRepeatCount(2);
        colorAnimation.setDuration(500); // milliseconds
        colorAnimation.addUpdateListener(animator ->
                mAnimatableBackground.setBackgroundColor((int) animator.getAnimatedValue()));
        colorAnimation.start();
        playAlertSound();
    }

    private void playAlertSound() {
        mp = MediaPlayer.create(getContext(), R.raw.alert_sound);
        mp.start();
    }

    private boolean isDriveActionCurret() {
        return mDriverButton.isChecked();
    }

    private boolean isRelaxActionCurret() {
        return mRestButton.isChecked();
    }

    private void setTimerString(String string) {
        if (mDriveRemainingTime != null) {
            mDriveRemainingTime.setText(string);
        }
    }

    @NonNull
    private String formatTimeString(long millisUntilFinished) {
        int seconds = (int) (millisUntilFinished / TIMER_INTERVAL_MS);
        final int hours = seconds / (60 * 60);
        final int tempMint = (seconds - (hours * 60 * 60));
        final int minutes = tempMint / 60;
        seconds = tempMint - (minutes * 60);
        return String.format(Locale.getDefault(), "%02d", hours)
                + ":" + String.format(Locale.getDefault(), "%02d", minutes)
                + ":" + String.format(Locale.getDefault(), "%02d", seconds);
    }

    @Override
    public void updateDriveTimer(long remaingTime) {
        setDriveCountDonwnTimer(remaingTime);
    }

    private void sendDriveViolation() {
        if (isDriveActionCurret()) {
            showWarningMessage(ThisApplication.getInstance().getString(R.string.violation));
            showErrorAlert();
        }
    }

    private void showWarningMessage(String msg) {
        startWarningAnimation();
        mWarningText.setText(msg);
    }
}
