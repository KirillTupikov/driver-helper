package com.driverhelp.fragment.history;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.driverhelp.R;
import com.driverhelp.data.Action;
import com.driverhelp.data.ActionType;
import com.driverhelp.utils.BaseRecyclerViewAdapter;

import static com.driverhelp.utils.DateUtils.getDurationTime;

public class HistoryItemAdapter extends BaseRecyclerViewAdapter<Action, HistoryItemAdapter.ItemEmptyHolder> {

    public static final int EMPTY_VIEW_TYPE = 1;
    public static final int EVENT_VIEW_TYPE = 2;

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).getActionType() == ActionType.EMPTY ? EMPTY_VIEW_TYPE : EVENT_VIEW_TYPE;
    }

    @Override
    public ItemEmptyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view;
        switch (viewType) {
            case EMPTY_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_empty_view, parent, false);
                return new ItemEmptyHolder(view);

            case EVENT_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_view, parent, false);
                return new ItemHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(ItemEmptyHolder holder, int position) {

        final Action action = mData.get(position);

        if (holder instanceof ItemHolder) {
            ItemHolder itemHolder = (ItemHolder) holder;
            itemHolder.icon.setImageResource(action.getActionType().getIconMinRes());
            if (action.getEndTime() != 0) {
                itemHolder.title.setText(getDurationTime(action.getRealTimeDuration()));
            } else {
                itemHolder.title.setText(R.string.in_process);
            }
        }
    }

    public static class ItemEmptyHolder extends RecyclerView.ViewHolder {

        public ItemEmptyHolder(View itemView) {
            super(itemView);
        }
    }

    public static class ItemHolder extends ItemEmptyHolder {

        public ImageView icon;
        public TextView title;

        public ItemHolder(View itemView) {
            super(itemView);

            icon = itemView.findViewById(R.id.item_icon);
            title = itemView.findViewById(R.id.item_time);
        }
    }
}
