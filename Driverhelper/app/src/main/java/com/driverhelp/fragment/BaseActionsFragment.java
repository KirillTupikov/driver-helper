package com.driverhelp.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.driverhelp.arch.ActionsViewModel;
import com.driverhelp.data.Action;
import com.driverhelp.data.message.MessageAction;
import com.driverhelp.utils.EventBusHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public abstract class BaseActionsFragment extends BaseFragment {

    private static final String TAG = "Logic- " + BaseActionsFragment.class.getSimpleName();

    protected ActionsViewModel mActionsViewModel;

//    public final static String BROADCAST_ACTION = "com.driverhelp.fragment.service_monitoring_action";

//    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (TextUtils.equals(action, BROADCAST_ACTION)) {
//                MessageAction messageAction =
//                        (MessageAction) intent.getSerializableExtra(TrackingActionService.ACTION_MESSAGE);
//                Log.i(TAG, "Alert message ("
//                        + messageAction.getAction().getActionType().name() +
//                        "): " + DateUtils.getCurrentTime(System.currentTimeMillis()));
//                handleMessageFromService(messageAction);
//            }
//        }
//    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
//        if (getActivity() != null) {
//            getActivity().registerReceiver(mBroadcastReceiver, intentFilter);
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (getActivity() != null) {
//            getActivity().unregisterReceiver(mBroadcastReceiver);
//        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mActionsViewModel = ViewModelProviders.of(this).get(ActionsViewModel.class);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected abstract void handleMessageFromService(MessageAction messageTime);

    @Subscribe
    public void onEvent(EventBusHelper.DriverEvent driverEvent) {
        if (driverEvent != null) {
            onNewEvent(driverEvent.getAction());
        }
    }

    @Subscribe
    public void onEvent(EventBusHelper.MessageActionEvent actionEvent) {
        if (actionEvent != null && getActivity() != null) {
            getActivity().runOnUiThread(() -> handleMessageFromService(actionEvent.getMessageAction()));
        }
    }

    @Deprecated
    protected void sendEvent(Action action) {
        EventBus.getDefault().post(new EventBusHelper.DriverEvent(action));
    }

    protected abstract void onNewEvent(Action action);

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }
}
