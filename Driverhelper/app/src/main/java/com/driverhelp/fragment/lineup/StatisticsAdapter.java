package com.driverhelp.fragment.lineup;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.driverhelp.R;
import com.driverhelp.data.Action;
import com.driverhelp.data.ActionType;
import com.driverhelp.data.BaseTimeModel;
import com.driverhelp.data.Day;
import com.driverhelp.logic.Const;
import com.driverhelp.utils.BaseRecyclerViewAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.driverhelp.utils.DateUtils.getDurationTime;

public class StatisticsAdapter extends BaseRecyclerViewAdapter<BaseTimeModel, RecyclerView.ViewHolder> {

    public static final int ITEM_VIEW_TYPE = 1;
    public static final int HEADER_VIEW_TYPE = 2;

    private SimpleDateFormat mFormatter;

    public StatisticsAdapter() {
        mFormatter = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position) instanceof Action ? ITEM_VIEW_TYPE : HEADER_VIEW_TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        View view;
        switch (type) {
            case ITEM_VIEW_TYPE:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_event, viewGroup, false);
                return new EventHolder(view);

            case HEADER_VIEW_TYPE:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_header, viewGroup, false);

                return new HeaderHolder(view);

            default:
                throw new IllegalArgumentException("Bad view type");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {

        if (holder instanceof EventHolder) {
            EventHolder eventHolder = (EventHolder) holder;
            Action action = (Action) mData.get(i);
            ActionType eventType = action.getActionType();

            eventHolder.icon.setImageResource(eventType.getIconMinRes());
            eventHolder.title.setText(eventType.getTitleRes());

            Date eventStartDate = new Date(action.getStartTime());
            eventHolder.time.setText(mFormatter.format(eventStartDate));
            if (action.getActionType() == ActionType.REST) {
                eventHolder.duration.setText(getDurationTime(action.getActualDuration()));
            } else {
                eventHolder.duration.setText(getDurationTime(action.getRealTimeDuration() * Const.TEST_COF));
            }
            eventHolder.realDuration.setText(getDurationTime(action.getRealTimeDuration() * Const.TEST_COF));
        } else if (holder instanceof HeaderHolder) {
            HeaderHolder headerHolder = (HeaderHolder) holder;
            Day day = (Day) mData.get(i);

            headerHolder.allSum.setText(getDurationTime(day.sumAll * Const.TEST_COF));
            headerHolder.driveSum.setText(getDurationTime(day.sumDrive * Const.TEST_COF));
            headerHolder.relaxSum.setText(getDurationTime(day.sumRelax));
            headerHolder.workSum.setText(getDurationTime(day.sumWork * Const.TEST_COF));
            headerHolder.poaSum.setText(getDurationTime(day.sumPoa * Const.TEST_COF));
            headerHolder.relaxRealSum.setText(getDurationTime(day.sumRealRelax * Const.TEST_COF));
        }
    }

    static class HeaderHolder extends RecyclerView.ViewHolder {
        public final TextView allSum;
        public final TextView driveSum;
        public final TextView workSum;
        public final TextView poaSum;
        public final TextView relaxSum;
        public final TextView relaxRealSum;

        public HeaderHolder(View itemView) {
            super(itemView);
            allSum = itemView.findViewById(R.id.sum_value);
            workSum = itemView.findViewById(R.id.work_value);
            poaSum = itemView.findViewById(R.id.poa_value);
            driveSum = itemView.findViewById(R.id.drive_value);
            relaxSum = itemView.findViewById(R.id.relax_value);
            relaxRealSum = itemView.findViewById(R.id.relax_value_real);
        }
    }

    static class EventHolder extends RecyclerView.ViewHolder {

        public final ImageView icon;
        public final TextView title;
        public final TextView time;
        public final TextView duration;
        public final TextView realDuration;

        EventHolder(View itemView) {
            super(itemView);

            icon = itemView.findViewById(R.id.icon);
            time = itemView.findViewById(R.id.starttime);
            title = itemView.findViewById(R.id.title);
            duration = itemView.findViewById(R.id.duration);
            realDuration = itemView.findViewById(R.id.duration_real);
        }
    }
}
