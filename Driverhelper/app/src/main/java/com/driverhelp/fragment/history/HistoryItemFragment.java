package com.driverhelp.fragment.history;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.driverhelp.R;
import com.driverhelp.data.Action;
import com.driverhelp.data.ActionType;
import com.driverhelp.data.database.DataBaseManager;
import com.driverhelp.data.message.MessageAction;
import com.driverhelp.fragment.BaseActionsFragment;

import java.util.ArrayList;

public class HistoryItemFragment extends BaseActionsFragment {

    public static HistoryItemFragment newInstance() {

        Bundle args = new Bundle();

        HistoryItemFragment fragment = new HistoryItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private HistoryItemAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_history_item;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final RecyclerView mRecyclerView = view.findViewById(R.id.recycler);

        mAdapter = new HistoryItemAdapter();
        mRecyclerView.setAdapter(mAdapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 4, GridLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.addItemDecoration(new ItemDecoration(getContext(), R.drawable.history_divider));

        mActionsViewModel.getActionsData().observe(
                this, actions -> mAdapter.setData(createHistoryList()));
    }

    private ArrayList<Action> createHistoryList() {
        ArrayList<Action> historyActions = new ArrayList<>();

        for (Action action : DataBaseManager.getAllActions()) {
            switch (action.getActionType()) {
                case DRIVE:
                    historyActions.add(action);
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    break;
                case WORK:
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    historyActions.add(action);
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    break;
                case POA:
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    historyActions.add(action);
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    break;
                case REST:
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    historyActions.add(new Action(ActionType.EMPTY.ordinal()));
                    historyActions.add(action);
                    break;
            }
        }

        return historyActions;
    }

    @Override
    protected void handleMessageFromService(MessageAction messageAction) {

    }

    @Override
    protected void onNewEvent(Action action) {
        mAdapter.setData(createHistoryList());
    }
}
