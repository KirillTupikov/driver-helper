package com.driverhelp.fragment.history;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.driverhelp.R;
import com.driverhelp.data.Action;
import com.driverhelp.data.message.MessageAction;
import com.driverhelp.fragment.BaseActionsFragment;
import com.driverhelp.utils.FragmentManagingHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class HistoryFragment extends BaseActionsFragment {

    private static final int MAX_DAYS = 7;

    public static HistoryFragment newInstance() {

        Bundle args = new Bundle();

        HistoryFragment fragment = new HistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private TabLayout mDaysTabLayout;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_history;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDaysTabLayout = view.findViewById(R.id.dayTabLayout);

        initDaysTabs();

//        TabLayout.Tab tab = mDaysTabLayout.getTabAt(0);
//        tab.select();

        FragmentManagingHelper.replaceFragment(getChildFragmentManager(), HistoryItemFragment.newInstance());
    }

    private static Calendar getCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    private void initDaysTabs() {
        if (getActivity() == null) {
            return;
        }

        Calendar date = getCalendar();

        SimpleDateFormat num = new SimpleDateFormat("d", new Locale("ru"));
        SimpleDateFormat day = new SimpleDateFormat("E", new Locale("ru"));

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < MAX_DAYS; i++) {
            // select first tab

            View view = inflater.inflate(R.layout.schedule_tab, mDaysTabLayout, false);

            String dayNum = num.format(date.getTime());
            ((TextView) view.findViewById(R.id.text1)).setText(dayNum);
            TextView scheduleDay = view.findViewById(R.id.text2);
            if (i != 0) {
                // android bug with week day names - shows number instead of day name
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    scheduleDay.setText(date.getTime().toString().split(" ")[0]);
                } else {
                    scheduleDay.setText(day.format(date.getTime()));
                }
            } else {
                scheduleDay.setText(R.string.today);
                scheduleDay.setEllipsize(TextUtils.TruncateAt.END);
//                scheduleDay.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Oswald-Regular.ttf"), Typeface.BOLD);
//                scheduleDay.setTextColor(ContextCompat.getColor(getContext(), R.color.app_violet_text_schedule_date));
            }

            TabLayout.Tab tab = mDaysTabLayout.newTab();
            mDaysTabLayout.addTab(tab);
            tab.setCustomView(view);

            if (i == 0) {
                tab.select();
                tab.getCustomView().setSelected(true);
            }

            tab.setTag(date);

            date.roll(Calendar.DAY_OF_MONTH, true);
            if (dayNum.equals(date.getActualMaximum(Calendar.DAY_OF_MONTH) + "")) {
                date.roll(Calendar.MONTH, true);
                if (date.getActualMaximum(Calendar.MONTH) == 12) {
                    date.roll(Calendar.YEAR, true);
                }
            }
        }
        mDaysTabLayout.addOnTabSelectedListener(onDaySelectedListener);
    }

    private TabLayout.OnTabSelectedListener onDaySelectedListener = new TabLayout.OnTabSelectedListener() {

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            Calendar date = (Calendar) tab.getTag();
            FragmentManagingHelper.replaceFragment(getChildFragmentManager(), HistoryItemFragment.newInstance());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    @Override
    protected void handleMessageFromService(MessageAction messageAction) {
    }

    @Override
    protected void onNewEvent(Action action) {

    }
}
