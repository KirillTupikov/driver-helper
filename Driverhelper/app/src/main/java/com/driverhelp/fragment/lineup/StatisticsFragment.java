package com.driverhelp.fragment.lineup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.driverhelp.R;
import com.driverhelp.data.Action;
import com.driverhelp.data.ActionType;
import com.driverhelp.data.BaseTimeModel;
import com.driverhelp.data.Day;
import com.driverhelp.data.database.DataBaseManager;
import com.driverhelp.data.message.MessageAction;
import com.driverhelp.fragment.BaseActionsFragment;
import com.driverhelp.logic.LogicManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StatisticsFragment extends BaseActionsFragment {

    public static StatisticsFragment newInstance() {

        Bundle args = new Bundle();
        StatisticsFragment fragment = new StatisticsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private RecyclerView mRecyclerView;
    private StatisticsAdapter mLineUpAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_lineup;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLineUpAdapter = new StatisticsAdapter();
        mRecyclerView = view.findViewById(R.id.recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mLineUpAdapter);

        final FragmentActivity activity = getActivity();
        if (activity != null) {
            mActionsViewModel.getActionsData().observe(activity, actions -> {
                if (actions == null) {
                    return;
                }

                Collections.reverse(actions);

                mLineUpAdapter.setData(getEvents());
            });
        }
    }

    @Override
    protected void handleMessageFromService(MessageAction messageAction) {
    }

    @Override
    public void onNewEvent(Action action) {
        mLineUpAdapter.setData(getEvents());
    }

    private List<BaseTimeModel> getEvents() {
        List<BaseTimeModel> baseTimeModels = new ArrayList<>();
        List<Day> days = LogicManager.getInstance().getAllDaysForLastSession();
        for (Day day : days) {
            List<Action> actions = DataBaseManager.getActionsForDay(day.getUid());
            for (Action action : actions) {
                if (action.getActionType() == ActionType.DRIVE) {
                    day.sumDrive = day.sumDrive + action.getRealTimeDuration();
                } else if (action.getActionType() == ActionType.REST) {
                    day.sumRelax = day.sumRelax + action.getActualDuration();
                    day.sumRealRelax = day.sumRealRelax + action.getRealTimeDuration();
                } else if(action.getActionType() == ActionType.POA) {
                    day.sumPoa = day.sumPoa + action.getRealTimeDuration();
                } else if(action.getActionType() == ActionType.WORK) {
                    day.sumWork = day.sumWork + action.getRealTimeDuration();
                }
                day.sumAll = day.sumAll + action.getRealTimeDuration();
            }

            baseTimeModels.add(day);
            baseTimeModels.addAll(actions);
        }

        return baseTimeModels;

//        ArrayList<Action> mActions = new ArrayList<>(DataBaseManager.getAllActions());
//        Collections.reverse(mActions);
//        return mActions;
    }
}
