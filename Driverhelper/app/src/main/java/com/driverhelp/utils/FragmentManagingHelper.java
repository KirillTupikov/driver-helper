package com.driverhelp.utils;

import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.driverhelp.R;

import java.util.List;

public class FragmentManagingHelper {

    public static void replaceFragment(FragmentManager manager, Fragment fragment) {
        String fragmentTag = fragment.getClass().getName();

        FragmentTransaction ft = manager.beginTransaction();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {

            //remove old fragment
            Fragment oldFragment = manager.findFragmentByTag(fragmentTag);
            if (oldFragment != null) {
                ft.remove(oldFragment);
            }

            ft.add(R.id.fragment_container, fragment, fragmentTag);
        } else {
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
        }

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    public static void addFragment(FragmentManager manager, Fragment fragment) {
        String fragmentTag = fragment.getClass().getName();
        FragmentTransaction ft = manager.beginTransaction();

        ft.add(R.id.fragment_container, fragment, fragmentTag);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(fragmentTag);

        ft.commitAllowingStateLoss();
    }

    public static void addBackStackFragment(FragmentManager manager, Fragment fragment) {
        String fragmentTag = fragment.getClass().getName();
        Fragment fragmentHide = manager.findFragmentByTag(fragmentTag);

        if (fragmentHide == null) {

            Fragment visibleFr = getVisibleFragment(manager);
            if (visibleFr != null) {
                FragmentTransaction ft = manager.beginTransaction();
                ft.hide(visibleFr);
                ft.commit();
            }

            addFragment(manager, fragment);
        } else {
            hideAndShowFragment(manager, fragmentHide);
        }
    }

    public static void hideAndShowFragment(FragmentManager manager, Fragment fragmentHide) {
        for (Fragment fragment : manager.getFragments()) {
            FragmentTransaction ft = manager.beginTransaction();
            if (fragment != null && !fragment.isHidden()) {
                ft.hide(fragment);
                ft.commit();
            }
        }

        FragmentTransaction ft = manager.beginTransaction();
        ft.show(fragmentHide);
        ft.commit();
    }

    public static Fragment getVisibleFragment(FragmentManager fragmentManager) {
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible()) {
                    return fragment;
                }
            }
        }

        return null;
    }
}
