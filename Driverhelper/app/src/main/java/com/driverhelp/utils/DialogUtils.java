package com.driverhelp.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.RadioGroup;

import com.driverhelp.R;

/**
 * Created by Sant on 25.11.2017.
 */

public class DialogUtils {

    private static AlertDialog dialog;

    private DialogUtils() {
    }


    public static void showOKDialog(Context context, String title, String message, @Nullable final CallBack callBack) {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
        dialog = new android.support.v7.app.AlertDialog.Builder(context)
                .setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton(R.string.ok_title, (dialogInterface, i) -> {
                    if (callBack != null) {
                        callBack.invoke();
                    }
                    dialogInterface.cancel();
                }).show();
    }

    public static void showChoiceDialog(Context context,
                                        String message,
                                        String lLabel,
                                        String rLabel,
                                        @Nullable final CallBack lCallBack,
                                        @Nullable final CallBack rCallBack
    ) {
        new android.support.v7.app.AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(rLabel, (dialogInterface, i) -> {
                    if (rCallBack != null) {
                        rCallBack.invoke();
                    }
                    dialogInterface.cancel();
                })
                .setNegativeButton(lLabel, (dialogInterface, i) -> {
                    if (lCallBack != null) {
                        lCallBack.invoke();
                    }
                    dialogInterface.cancel();
                }).show();
    }

    public static void showWorkTypeDialog(Context context,
                                          @Nullable final RadioButtonCallBack okCallBack,
                                          @Nullable CharSequence title,
                                          @Nullable CallBack cancelCallBack) {
        View radioDialogView = View.inflate(context, R.layout.work_type_dialog, null);
        RadioGroup radioGroupDialog = radioDialogView.findViewById(R.id.radio_group_work_type);
        showRadioButtonDialog(context, okCallBack, cancelCallBack, title, radioDialogView, radioGroupDialog);
    }

//Диалог выбора перерыва
    public static void showRestTypeDialog(Context context,
                                          @Nullable final RadioButtonCallBack okCallBack,
                                          @Nullable CallBack cancelCallBack) {
        View radioDialogView = View.inflate(context, R.layout.rest_radio_dialog, null);
        RadioGroup radioGroupDialog = radioDialogView.findViewById(R.id.radio_group);
        showRadioButtonDialog(context, okCallBack, cancelCallBack, null, radioDialogView, radioGroupDialog);
    }

    public static void showRadioButtonDialog(Context context,
                                             @Nullable final RadioButtonCallBack okCallBack,
                                             @Nullable CallBack cancelCallBack,
                                             @Nullable CharSequence title,
                                             @NonNull View radioDialogView,
                                             @NonNull RadioGroup radioGroupDialog
    ) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setCancelable(false)
                .setView(radioDialogView)
                .setPositiveButton(context.getString(R.string.ok_title), (dialogInterface, i) -> {
                    if (okCallBack != null) {
                        okCallBack.invoke(radioGroupDialog.getCheckedRadioButtonId());
                    }
                    dialogInterface.cancel();
                })
                .setNegativeButton(context.getString(R.string.cancel), (dialogInterface, i) -> {
                    if (cancelCallBack != null) {
                        cancelCallBack.invoke();
                    }
                    dialogInterface.cancel();
                }).show();
    }

    /*
     * * Creates a confirmation dialog.
            *
            * @param context        the context
     * @param message        the message
     * @param cancelCallBack the callback for cancel button
     * @param okCallBack     the callback for ok button
     */

    public static void showOKCancelDialog(Context context,
                                          int message,
                                          final CallBack cancelCallBack,
                                          final CallBack okCallBack
    ) {
        new android.support.v7.app.AlertDialog.Builder(context)
                .setMessage(context.getResources().getString(message))
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    if (cancelCallBack != null) {
                        cancelCallBack.invoke();
                    }
                })
                .setPositiveButton(R.string.ok_title, (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    if (okCallBack != null) {
                        okCallBack.invoke();
                    }
                }).show();
    }

    /**
     * Represents a command that can be executed.
     */
    public interface CallBack {
        void invoke();
    }

    public interface RadioButtonCallBack {
        void invoke(int id);
    }

}
