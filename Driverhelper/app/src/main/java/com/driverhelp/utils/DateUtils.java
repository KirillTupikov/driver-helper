package com.driverhelp.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import static java.util.SimpleTimeZone.UTC_TIME;

/**
 * Created by Helm on 26.11.2017.
 */

public class DateUtils {

    public static String getDurationTime(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(" HH:mm:ss", Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(new Date(time));
    }

    public static String getCurrentTime(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(" HH:mm:ss", Locale.ENGLISH);
        return simpleDateFormat.format(new Date(time));
    }
}
