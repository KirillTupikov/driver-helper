package com.driverhelp.utils;

import android.content.Context;

import com.driverhelp.data.message.MessageAction;

/**
 * Created by ASUS on 26.11.2017.
 */

public class MessageUtils {

    public static void showMessage(Context context, MessageAction messageAction, DialogUtils.CallBack call) {
        String title = context.getString(messageAction.getAction().getActionType().getTitleRes()).toUpperCase();
        String description = messageAction.getMessage();

        switch (messageAction.getMessageType()) {
            case DIALOG:
                DialogUtils.showOKDialog(context, title, description, call);
                break;

            case MESSAGE:
                DialogUtils.showOKDialog(context, title, description, call);
                break;

            case NOTIFICATION:
                DialogUtils.showOKDialog(context, title, description, call);
                break;
        }
    }
}
