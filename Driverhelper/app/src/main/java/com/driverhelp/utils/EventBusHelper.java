package com.driverhelp.utils;

import android.location.Location;

import com.driverhelp.data.Action;
import com.driverhelp.data.GPSData;
import com.driverhelp.data.message.MessageAction;

public class EventBusHelper {

    public static class GPSEvent {
        private GPSData gpsData;

        public GPSEvent(boolean isEnabled) {
            gpsData = new GPSData(isEnabled);
        }

        public GPSEvent(Location location) {
            gpsData = new GPSData(location);
        }

        public GPSData getData() {
            return this.gpsData;
        }
    }

    public static class EndSessionEvent {
        boolean isService;

        public boolean isService() {
            return isService;
        }

        public EndSessionEvent(boolean isService) {
            this.isService = isService;
        }
    }

    public static class ActivityTypeEvent {
        private String activityType;

        public ActivityTypeEvent(String activityType) {
            this.activityType = activityType;
        }

        public String getData() {
            return this.activityType;
        }

    }

    public static class DriverEvent {
        private Action action;

        public DriverEvent(Action action) {
            this.action = action;
        }

        public Action getAction() {
            return action;
        }
    }

    public static class MessageActionEvent {
        private MessageAction messageAction;

        public MessageActionEvent(MessageAction messageAction) {
            this.messageAction = messageAction;
        }

        public MessageAction getMessageAction() {
            return messageAction;
        }
    }
}
