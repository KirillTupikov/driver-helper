package com.driverhelp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.driverhelp.logic.Const;

public class PreferenceHelper {

    public static final String TEST_CONF = "TEST_CONF";
    private static final String PREFS = "PREFS";

    public static int getTestCof(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        return prefs.getInt(TEST_CONF, 250);
    }

    public static void setTestCof(Context context, int value) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(TEST_CONF, value);
        edit.apply();
        Const.reGenerate(context);
    }
}
