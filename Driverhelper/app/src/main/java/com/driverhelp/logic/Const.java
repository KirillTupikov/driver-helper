package com.driverhelp.logic;

import android.content.Context;

import com.driverhelp.utils.PreferenceHelper;

import static android.text.format.DateUtils.HOUR_IN_MILLIS;
import static android.text.format.DateUtils.MINUTE_IN_MILLIS;
import static android.text.format.DateUtils.SECOND_IN_MILLIS;

public class Const {

    public static int TEST_COF = 1;

    public static void reGenerate(Context context) {
        TEST_COF = PreferenceHelper.getTestCof(context);
        TIMER_INTERVAL_MS = 1000 / TEST_COF;
        DRIVE_TIME_MAX = DRIVE_TIME_MAX_DEV / TEST_COF;
        RELAX_SHORT_TIME = RELAX_TIME_MAX_DEV / TEST_COF;
        RELAX_SHORT_TIME_MIN = SHORT_RELAX_MIN_DEV / TEST_COF;
        RELAX_SHORT_TIME_MAX = SHORT_RELAX_MAX_DEV / TEST_COF;
        DAY_SESSION_MAX = DAY_SESSION_MAX_DEV / TEST_COF;
        ALERT_PERIOD = ALERT_PERIOD_DEV / TEST_COF;

        RELAX_3_HOURS = RELAX_3_HOURS_DEV / TEST_COF;
        RELAX_9_HOURS = RELAX_9_HOURS_DEV / TEST_COF;
        RELAX_11_HOURS = RELAX_11_HOURS_DEV / TEST_COF;

        DRIVE_9_HOURS = DRIVE_9_HOURS_DEV / TEST_COF;
        DRIVE_10_HOURS = DRIVE_10_HOURS_DEV / TEST_COF;

        TEST_HOUR = 60 * MINUTE_IN_MILLIS / TEST_COF;
    }

    public static float TIMER_INTERVAL_MS = 1000 / TEST_COF;
    public static long TEST_HOUR = 60 * MINUTE_IN_MILLIS / TEST_COF;
    private static final long ALERT_PERIOD_DEV = 60 * SECOND_IN_MILLIS;
    private static final long NOTIF_PERIOD_DEV = 60 * SECOND_IN_MILLIS;
    private static final long DRIVE_TIME_MAX_DEV = 4 * HOUR_IN_MILLIS + 30 * MINUTE_IN_MILLIS;
    public static final long RELAX_TIME_MAX_DEV = 45 * MINUTE_IN_MILLIS;
    public static final long SHORT_RELAX_MIN_DEV = 15 * MINUTE_IN_MILLIS;
    public static final long SHORT_RELAX_MAX_DEV = 30 * MINUTE_IN_MILLIS;
    private static final long DAY_SESSION_MAX_DEV = 24 * HOUR_IN_MILLIS;

    public static final long RELAX_3_HOURS_DEV = 3 * HOUR_IN_MILLIS;
    public static final long RELAX_9_HOURS_DEV = 9 * HOUR_IN_MILLIS;
    public static final long RELAX_11_HOURS_DEV = 11 * HOUR_IN_MILLIS;

    public static final long DRIVE_9_HOURS_DEV = 9 * HOUR_IN_MILLIS;
    public static final long DRIVE_10_HOURS_DEV = 10 * HOUR_IN_MILLIS;

    //DRIVE
    public static long DRIVE_TIME_MAX = DRIVE_TIME_MAX_DEV / TEST_COF;

    //REST
    public static long RELAX_SHORT_TIME = RELAX_TIME_MAX_DEV / Const.TEST_COF;
    public static long RELAX_SHORT_TIME_MIN = SHORT_RELAX_MIN_DEV / Const.TEST_COF;
    public static long RELAX_SHORT_TIME_MAX = SHORT_RELAX_MAX_DEV / Const.TEST_COF;

    //SESSION
    public static long DAY_SESSION_MAX = DAY_SESSION_MAX_DEV;

    //DIALOGS
    public static long ALERT_PERIOD = ALERT_PERIOD_DEV / TEST_COF;
    public static final long NOTIF_PERIOD = NOTIF_PERIOD_DEV / 10;

    public static long RELAX_3_HOURS = RELAX_3_HOURS_DEV / Const.TEST_COF;
    public static long RELAX_9_HOURS = RELAX_9_HOURS_DEV / Const.TEST_COF;
    public static long RELAX_11_HOURS = RELAX_11_HOURS_DEV / Const.TEST_COF;

    public static long DRIVE_9_HOURS = DRIVE_9_HOURS_DEV / Const.TEST_COF;
    public static long DRIVE_10_HOURS = DRIVE_10_HOURS_DEV / Const.TEST_COF;
}
