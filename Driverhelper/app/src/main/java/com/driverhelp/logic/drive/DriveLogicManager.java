package com.driverhelp.logic.drive;

public class DriveLogicManager {

    private static volatile DriveLogicManager mInstance;

    public static DriveLogicManager getInstance() {

        DriveLogicManager localInstance = mInstance;
        if (localInstance == null) {
            synchronized (DriveLogicManager.class) {
                localInstance = mInstance;
                if (localInstance == null) {
                    mInstance = localInstance = new DriveLogicManager();
                }
            }
        }

        return localInstance;
    }
}
