package com.driverhelp.logic;

import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import com.driverhelp.R;
import com.driverhelp.ThisApplication;
import com.driverhelp.data.Action;
import com.driverhelp.data.ActionType;
import com.driverhelp.data.Day;
import com.driverhelp.data.RestType;
import com.driverhelp.data.Week;
import com.driverhelp.data.WorkSession;
import com.driverhelp.data.database.DataBaseManager;
import com.driverhelp.data.message.MessageTime;
import com.driverhelp.data.message.MessageType;
import com.driverhelp.logic.services.TrackingActionService;
import com.driverhelp.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

import static com.driverhelp.logic.Const.TEST_HOUR;

public class LogicManager {

    private static final String TAG = "Logic- " + LogicManager.class.getSimpleName();

    private static volatile LogicManager mInstance;
    private static boolean isNineType;
    private UpdateTimerListener updateTimerListener;

    public static LogicManager getInstance() {
        LogicManager localInstance = mInstance;
        if (localInstance == null) {
            synchronized (LogicManager.class) {
                localInstance = mInstance;
                if (localInstance == null) {
                    mInstance = localInstance = new LogicManager();
                }
            }
        }

        return localInstance;
    }

    public LogicManager() {
        getLastSessionWithInitData();
    }

    private WorkSession mLastSession;
    private Week mCurrentWeek;
    private Day mCurrentDay;
    private Action mCurrentAction;

    @Nullable
    private WorkSession getLastSessionWithInitData() {
        if (mLastSession == null) {
            mLastSession = getLastNotEndWorkSession();
            if (mLastSession != null) {
                mCurrentWeek = DataBaseManager.getNotEndWeekForWorkSession(mLastSession.getUid());
                mCurrentDay = DataBaseManager.getNotEndDayForWeek(mCurrentWeek.getUid());
                mCurrentAction = DataBaseManager.getNotEndActionForDay(mCurrentDay.getUid());
            }
        }
        if (mLastSession != null) {
            //endTime = 0 it means worksession not ended
            return mLastSession.getEndTime() == 0 ? mLastSession : null;
        }
        return mLastSession;

    }

    @Nullable
    public Week getCurrentWeek() {
        return mCurrentWeek;
    }

    @Nullable
    public Day getCurrentDay() {
        return mCurrentDay;
    }

    @Nullable
    public Action getCurrentAction() {
        return mCurrentAction;
    }

    public void setWorkDayType(boolean isNine) {
        if (mCurrentDay != null) {
            mCurrentDay.setTenHours(isNine);
        }
    }

    private void startWorkSession(PostNewActionListener newActionListener) {
        //check if session already started
        if (getLastSessionWithInitData() != null) {
            return;
        }

        final long createTime = System.currentTimeMillis();
        Log.i(TAG, "Create work session: " + DateUtils.getCurrentTime(createTime));
        mLastSession = new WorkSession(SystemClock.elapsedRealtime());
        long uidWS = DataBaseManager.addWorkSession(mLastSession);
        mLastSession.setUid(uidWS);
        mCurrentWeek = new Week(createTime, uidWS);
        long uidW = DataBaseManager.addWeek(mCurrentWeek);
        mCurrentWeek.setUid(uidW);
        mCurrentDay = new Day(createTime, uidW);
        long uidD = DataBaseManager.addDay(mCurrentDay);
        mCurrentDay.setUid(uidD);
        if (newActionListener != null) {
            newActionListener.createdDay(true);
        }

        Log.d(TAG, "new Day " + mCurrentDay.getUid());
    }

    public boolean isTenHoursDay() {
        if (mCurrentDay != null) {
            return mCurrentDay.isTenHours;
        }

        return false;
    }

    public boolean isDriveTimeEnd() {
        if (!isTenHoursDay() && getDriveAllTimeForDay() > Const.DRIVE_9_HOURS) {
            return true;
        }

        if (isTenHoursDay() && getDriveAllTimeForDay() > Const.DRIVE_10_HOURS) {
            return true;
        }

        return false;
    }

    public boolean isWorkSessionActive() {
        WorkSession lastSession = getLastSessionWithInitData();
        return lastSession != null && lastSession.getEndTime() == 0;
    }

    public void stopWorkSession() {
        WorkSession worksession = getLastSessionWithInitData();
        if (worksession != null) {
            endAndUpdateWorkSession(worksession);
            TrackingActionService.stopService();
        }
    }

    @Nullable
    private WorkSession getLastNotEndWorkSession() {
        List<WorkSession> workSessions = DataBaseManager.getNotEndSession();
        //If more then one
        for (int i = 0; i < workSessions.size() - 1; i++) {
            WorkSession endSession = workSessions.get(i);
            endAndUpdateWorkSession(endSession);
        }

        //Get last not end session
        if (!workSessions.isEmpty()) {
            return workSessions.get(workSessions.size() - 1);
        }

        return null;
    }

    public List<Day> getAllDaysForLastSession() {
        List<Day> days = new ArrayList<>();
        if (mLastSession != null) {
            List<Week> weeks = DataBaseManager.getWeeksForWorkSession(mLastSession.getUid());
            for (Week week : weeks) {
                days.addAll(DataBaseManager.getDaysForWeek(week.getUid()));
            }
        }

        return days;
    }

    private void endAndUpdateWorkSession(WorkSession workSession) {
        workSession.setEndTime(System.currentTimeMillis());
        DataBaseManager.updateWorkSession(workSession);
    }

    public Action getLastAction() {
        return mCurrentAction;
    }

    public void updateCurrentAction(Action action) {
        mCurrentAction = action;
        DataBaseManager.updateAction(action);
    }

    public Action addNewAction(ActionType newActionType, @Nullable RestType restType, PostNewActionListener newActionListener) {
        startWorkSession(newActionListener);

        final long createTime = System.currentTimeMillis();
        Log.i(TAG, "Add new action( " + newActionType.name() + ") :" + DateUtils.getCurrentTime(createTime));

        //Update last event
        Action action = DataBaseManager.getNotEndActionForDay(mCurrentDay.getUid());
        if (action != null) {
            action.setEndTime(createTime);
            endAction(action);
            DataBaseManager.updateAction(action);
        }

        //Check create new day
        List<Action> relaxActions = DataBaseManager.getActionsByTypeForDay(ActionType.REST, mCurrentDay.getUid());
        //If was 9 hours relax, will create new day
        for (Action relax : relaxActions) {
            if (relax.getActualDuration() >= Const.RELAX_9_HOURS_DEV) {
                mCurrentDay.setEndTime(createTime);
                DataBaseManager.updateDay(mCurrentDay);

                //Create new day
                mCurrentDay = new Day(createTime, mCurrentWeek.getUid());
                long uidD = DataBaseManager.addDay(mCurrentDay);
                mCurrentDay.setUid(uidD);

                //Check two ten days for a week
                List<Day> tenDays = DataBaseManager.getTenDaysForWeek(mCurrentWeek.getUid(), true);
                if (newActionListener != null) {
                    newActionListener.createdDay(tenDays.size() < 2);
                }

                Log.i(TAG, "Create new day :" + DateUtils.getCurrentTime(createTime));

                break;
            }
        }

        mCurrentAction = new Action(createTime, newActionType, mCurrentDay.getUid());
        if (newActionType == ActionType.REST) {
            mCurrentAction.setRestTypeEnum(restType);
        }

        long uid = DataBaseManager.addAction(mCurrentAction);
        mCurrentAction.setUid(uid);

        timeTimerCalculate(mCurrentAction);

        if (newActionListener != null) {
            newActionListener.createdAction(mCurrentAction);
        }

        TrackingActionService.startService(mCurrentAction);

//        calcTimeDuration(mCurrentAction);
        return mCurrentAction;
    }

    private void timeTimerCalculate(Action action) {
        switch (action.getActionType()) {
            case DRIVE:
                final long remainingDriveTime = getDriveRelaxTimeLeft();
                long time = Const.DRIVE_TIME_MAX - remainingDriveTime;
                if (getDriveTimeLeftForDay() - time > 0) {
                    updateDriveCountDownTimer(time);
                } else {
                    updateDriveCountDownTimer(getDriveTimeLeftForDay());
                }
                break;
        }
    }

    private void endAction(Action action) {
        long actionDuration = System.currentTimeMillis() - action.getStartTime();

        switch (action.getActionType()) {
            case DRIVE:
                final long sumDriveTime = getDriveRelaxTimeLeft();

                //Затем смотрим, сколько времени осталось на ивент, который мы проверяем
                if (action.getRealTimeDuration() + sumDriveTime > Const.DRIVE_TIME_MAX) {
                    action.setError(true);
                }

                //TODO add check with + 1 hour
                break;

            case WORK:

                break;

            case POA:

                break;

            case REST:

                //Проверяем на 15 минутные перерывы
                boolean isShortRelax = isShortRelax();
                if (isShortRelax) {
                    if (actionDuration < Const.RELAX_SHORT_TIME_MAX) {
                        action.setActualDuration(0);
                        break;
                    }

                    if (actionDuration >= Const.RELAX_SHORT_TIME_MAX) {
                        action.setActualDuration(Const.SHORT_RELAX_MAX_DEV);
                        break;
                    }
                } else {
                    if (actionDuration < Const.RELAX_SHORT_TIME_MIN) {
                        action.setActualDuration(0);
                        break;
                    }

                    if (actionDuration < Const.RELAX_SHORT_TIME) {
                        action.setActualDuration(Const.SHORT_RELAX_MIN_DEV);
                        break;
                    }
                }

                int nineHoursRelax = getNineRelaxInWeek();
                if (actionDuration < Const.RELAX_SHORT_TIME_MAX) {
                    action.setActualDuration(Const.SHORT_RELAX_MIN_DEV);
                } else if (actionDuration < Const.RELAX_SHORT_TIME) {
                    action.setActualDuration(Const.SHORT_RELAX_MAX_DEV);
                } else if (actionDuration < Const.RELAX_3_HOURS) {
                    action.setActualDuration(Const.RELAX_TIME_MAX_DEV);
                } else if (actionDuration < Const.RELAX_9_HOURS) {
                    action.setActualDuration(Const.RELAX_3_HOURS_DEV);
                } else if (actionDuration < Const.RELAX_11_HOURS && nineHoursRelax < 3) {
                    action.setActualDuration(Const.RELAX_9_HOURS_DEV);
                } else if (actionDuration < Const.RELAX_11_HOURS && nineHoursRelax >= 3) {
                    action.setActualDuration(Const.RELAX_3_HOURS_DEV); //TODO спросить какой фиксировать в такой ситуации
                } else {
                    action.setActualDuration(Const.RELAX_11_HOURS_DEV);
                }

                break;
        }
    }

    //Возвращает время вождения в 4,5 часовой период
    private long getDriveRelaxTimeLeft() {
        long sumDriveTime = 0;// Суммарное время вождения за день
        Log.d(TAG, "get action by day " + mCurrentDay.getUid());
        List<Action> driveRelaxActions = DataBaseManager.getActionsDriveRelaxForDay(mCurrentDay.getUid());
        if (!driveRelaxActions.isEmpty()) {
            long sumRelaxTime = 0;
            for (Action DRAction : driveRelaxActions) {
                if (DRAction.getActionType() == ActionType.DRIVE) {
                    sumDriveTime = sumDriveTime + DRAction.getRealTimeDuration();
                }

                if (DRAction.getActionType() == ActionType.REST) {
                    sumRelaxTime = sumRelaxTime + DRAction.getActualDuration();
                }

                //Как только суммарное время вождения становится больше положенного и отдых равен положенному
                // или больше мы обнуляем время первого периода вождения
                if (sumRelaxTime >= Const.RELAX_TIME_MAX_DEV) {
                    sumDriveTime = 0;
                    sumRelaxTime = 0;
                }
            }
            //TODO add check with + 1 hour
        }
        return sumDriveTime;
    }

    //Получение количества 9 часовых отдыхов в неделю
    private int getNineRelaxInWeek() {
        int nineDays = 0;
        List<Day> days = DataBaseManager.getDaysForWeek(mCurrentWeek.getUid());
        for (Day day : days) {
            List<Action> actions = DataBaseManager.getActionsByTypeForDay(ActionType.REST, day.getUid());
            for (Action relax : actions) {
                if (relax.getActualDuration() == Const.DRIVE_9_HOURS) {
                    nineDays++;
                }
            }
        }

        return nineDays;
    }

    //Оставшееся время вождения в дне с учетом смен
    private long getDriveTimeLeftForDay() {
        long driveDuration = getDriveAllTimeForDay();
        long dayMaxDriveDuration = mCurrentDay.isTenHours() ? 10 * TEST_HOUR : 9 * TEST_HOUR;

        long time = dayMaxDriveDuration - driveDuration;
        return time < 0 ? 0 : time;
    }

    public long getDriveAllTimeForDay() {
        List<Action> actions = DataBaseManager.getActionsByTypeForDay(ActionType.DRIVE, mCurrentDay.getUid());
        long driveDuration = 0;
        for (Action a : actions) {
            driveDuration = driveDuration + a.getRealTimeDuration();
        }
        return driveDuration;
    }

    public MessageTime calcTimeDuration(Action action) {
        long actionDuration = System.currentTimeMillis() - action.getStartTime();

        switch (action.getActionType()) {
            case DRIVE:

                long remainingDriveTime = getDriveRelaxTimeLeft();

                long time = Const.DRIVE_TIME_MAX - remainingDriveTime;
                if (getDriveTimeLeftForDay() - time > 0) {
                    remainingDriveTime = time;
                } else {
                    remainingDriveTime = getDriveTimeLeftForDay();
                }

                if (remainingDriveTime - Const.ALERT_PERIOD > 0) {
                    return new MessageTime(remainingDriveTime - Const.ALERT_PERIOD, MessageType.MESSAGE,
                            ThisApplication.getInstance().getString(R.string.dialog_message_drive_time));
                } else {
                    return new MessageTime(Const.NOTIF_PERIOD, MessageType.DIALOG,
                            ThisApplication.getInstance().getString(R.string.dialog_message_drive_time_end));
                }

            case WORK:

                break;

            case POA:

                //если  >= 45 мин предложить переключить в RELAX
                return new MessageTime(Const.RELAX_SHORT_TIME, MessageType.DIALOG, ThisApplication.getInstance().getString(R.string.dialog_message_poa_limit));

            case REST:

                //Для еженедельного отдыха пока ничего не трекаем
                if (action.getRestTypeEnum() == RestType.WEAK) {
                    return null;
                }

                if (action.getRestTypeEnum() == RestType.SHORT) {
                    //Проверяем на 15 минутные перерывы
                    boolean isShortRelax = isShortRelax();

                    if (!isShortRelax && actionDuration < Const.RELAX_SHORT_TIME_MIN) {
                        return new MessageTime((Const.RELAX_SHORT_TIME_MIN) - actionDuration, MessageType.MESSAGE,
                                ThisApplication.getInstance().getString(R.string.dialog_message_relax_time_min));
                    }

                    if (isShortRelax && actionDuration < Const.RELAX_SHORT_TIME_MAX) {
                        return new MessageTime((Const.RELAX_SHORT_TIME_MAX) - actionDuration, MessageType.MESSAGE,
                                ThisApplication.getInstance().getString(R.string.dialog_message_relax_time_max));
                    }

                    if (actionDuration < Const.RELAX_SHORT_TIME) {
                        return new MessageTime((Const.RELAX_SHORT_TIME) - actionDuration, MessageType.MESSAGE,
                                ThisApplication.getInstance().getString(R.string.dialog_message_relax_time_all));
                    }
                }

                if (action.getRestTypeEnum() != RestType.WEAK) {
                    if (actionDuration < Const.RELAX_3_HOURS) {
                        return new MessageTime((Const.RELAX_3_HOURS) - actionDuration, MessageType.MESSAGE,
                                ThisApplication.getInstance().getString(R.string.dialog_message_relax_time_three_hours));
                    }

                    int nineHoursRelax = getNineRelaxInWeek();
                    //Если 9 часовые отдыхи в неделе меньше 3
                    if (actionDuration < Const.RELAX_9_HOURS && nineHoursRelax < 3) {
                        return new MessageTime((Const.RELAX_9_HOURS) - actionDuration, MessageType.MESSAGE,
                                ThisApplication.getInstance().getString(R.string.dialog_message_relax_time_nine_hours));
                    }

                    if (actionDuration < Const.RELAX_11_HOURS) {
                        return new MessageTime((Const.RELAX_11_HOURS) - actionDuration, MessageType.MESSAGE,
                                ThisApplication.getInstance().getString(R.string.dialog_message_relax_time_eleven_hours));
                    }

                    return new MessageTime(Const.NOTIF_PERIOD, MessageType.MESSAGE,
                            ThisApplication.getInstance().getString(R.string.dialog_message_relax_time_eleven_hours));
                }
        }

        return new MessageTime(10000, MessageType.DIALOG, "");
    }


    private boolean isShortRelax() {
        //Проверяем на 15 минутные перерывы
        List<Action> relaxActions = DataBaseManager.getActionsByTypeForDay(ActionType.REST, mCurrentDay.getUid());
        boolean isShortRelax = false;
        for (Action relax : relaxActions) {
            if (relax.getActualDuration() == Const.SHORT_RELAX_MIN_DEV) {
                isShortRelax = true;
            } else if (relax.getActualDuration() != 0) {
                isShortRelax = false;
            }
        }

        return isShortRelax;
    }

    private void updateDriveCountDownTimer(long remainingTimeMs) {
        if (updateTimerListener != null) {
            updateTimerListener.updateDriveTimer(remainingTimeMs);
        }
    }

    public void clearUpdateTimerListener() {
        updateTimerListener = null;
    }

    public void setUpdateTimerListener(UpdateTimerListener updateTimerListener) {
        this.updateTimerListener = updateTimerListener;
    }


    public interface UpdateTimerListener {
        void updateDriveTimer(long remaingTime);

//        void updateRelaxTimer(long remaingTime);
    }

    public interface PostNewActionListener {
        void createdDay(boolean isTenAnswer);

        void createdAction(Action action);
    }
}
