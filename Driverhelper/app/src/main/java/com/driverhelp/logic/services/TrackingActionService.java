package com.driverhelp.logic.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.driverhelp.R;
import com.driverhelp.ThisApplication;
import com.driverhelp.activity.MainActivity;
import com.driverhelp.data.Action;
import com.driverhelp.data.message.MessageAction;
import com.driverhelp.data.message.MessageTime;
import com.driverhelp.logic.LogicManager;
import com.driverhelp.utils.DateUtils;
import com.driverhelp.utils.EventBusHelper;

import org.greenrobot.eventbus.EventBus;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

public class TrackingActionService extends JobIntentService {

    private static final String TAG = "Logic- " + TrackingActionService.class.getSimpleName();

    public static final String ACTION = "extra_action";
    public static final String ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE";
    public static final String ACTION_MESSAGE = "extra_action_message";

    public static final int FIXED_JOB_ID = 3737;
    public static final int ONGOING_NOTIFICATION_ID = 1000;

    public static void startService(Action action) {
        Intent intent = new Intent();
        intent.putExtra(ACTION, action);

        enqueueWork(ThisApplication.getInstance(), new ComponentName(ThisApplication.getInstance(), TrackingActionService.class), FIXED_JOB_ID, intent);
        Log.i(TAG, "Start service " + action.getActionType().name());
    }

    private static boolean isStarted = false;

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

        isStarted = true;

        Action action = (Action) intent.getSerializableExtra(ACTION);
        if (action == null) {
            return;
        }
        createNotification(action);

        while (isStarted) {
            MessageTime messageTime = LogicManager.getInstance().calcTimeDuration(action);
            if (messageTime == null) {
                stopService();
                return;
            }

            Log.i(TAG, "Duration time (" + action.getActionType().name() + " "
                    + messageTime.getType().name() + "): "
                    + DateUtils.getDurationTime(messageTime.getCalcDuration()));

            long endTime = System.currentTimeMillis();
            while (isStarted && (endTime + messageTime.getCalcDuration() > System.currentTimeMillis())) {
            }

            if (isStarted) {
                sendMessage(new MessageAction(action, messageTime.getType(), messageTime.getMessage()));
            }
        }

        //TODO Call pre messages

        //TODO Call notifications
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        isStarted = false;
        if (intent != null && intent.getAction() != null && intent.getAction().equals(ACTION_STOP_SERVICE)) {
            LogicManager.getInstance().stopWorkSession();
            EventBus.getDefault().post(new EventBusHelper.EndSessionEvent(true));
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void sendMessage(MessageAction messageAction) {
        EventBus.getDefault().post(new EventBusHelper.MessageActionEvent(messageAction));
//        Intent intent = new Intent();
//        intent.putExtra(ACTION_MESSAGE, messageAction);
//        sendBroadcast(intent);
    }

    private void createNotification(Action action) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Intent stopselfIntent = new Intent(this, TrackingActionService.class);
        stopselfIntent.setAction(ACTION_STOP_SERVICE);
        PendingIntent stopIntent = PendingIntent.getService(this, 0, stopselfIntent, FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(action.getActionType().getIconMinRes())
//                .setBadgeIconType(action.getActionType().getIconRes())
                .setContentTitle(getString(action.getActionType().getTitleRes()).toUpperCase())
                .setContentText(getString(R.string.in_process))
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.ic_pause, getString(R.string.stop_service), stopIntent)
                .build();


        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }

    final Handler mHandler = new Handler();

    // Helper for showing tests
    void toast(final CharSequence text) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TrackingActionService.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(), getString(R.string.end_work_session_toast), Toast.LENGTH_SHORT).show();
    }

    public static void stopService() {
        Log.i(TAG, "Stop service");
        isStarted = false;
    }
}
