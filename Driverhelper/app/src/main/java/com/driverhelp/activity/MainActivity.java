package com.driverhelp.activity;

import android.Manifest;
import android.arch.lifecycle.LifecycleOwner;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.driverhelp.R;
import com.driverhelp.arch.SomeObserver;
import com.driverhelp.fragment.history.HistoryFragment;
import com.driverhelp.fragment.lineup.StatisticsFragment;
import com.driverhelp.fragment.main.MainFragment;
import com.driverhelp.utils.DialogUtils;
import com.driverhelp.utils.EventBusHelper;
import com.driverhelp.utils.FragmentManagingHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.driverhelp.tracker.LocationTracker.validateGPSProvider;

public class MainActivity extends BaseActivity implements LifecycleOwner {

    public static final int EXIT_DELAY_MILLIS = 2000;
    private boolean doubleBackToExitPressedOnce;
    SomeObserver someObserver = new SomeObserver(getLifecycle(), SomeObserver.Owner.ACTIVITY);
    private View mGpsRequestLayout;
    private View mGpsPermissionLayout;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        mGpsRequestLayout = findViewById(R.id.gps_listener_layout);
        mGpsPermissionLayout = findViewById(R.id.gps_permission_layout);
        findViewById(R.id.go_app_settings).setOnClickListener(view -> navigateToAppSettings());

        if (!enableLocationTracking()) {
            mGpsPermissionLayout.setVisibility(VISIBLE);
            findViewById(R.id.fragment_container).setVisibility(GONE);
            findViewById(R.id.ok_request_gps).setOnClickListener(v -> enableLocationTracking());
        }
        BottomNavigationView navigationView = findViewById(R.id.bottom_navigation);
        navigationView.setOnNavigationItemSelectedListener(mNavListener);
        navigationView.setSelectedItemId(R.id.action_main);

        enableHome(false);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSION_ACCESS_COURSE_LOCATION) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                findViewById(R.id.gps_permission_layout).setVisibility(GONE);
                findViewById(R.id.fragment_container).setVisibility(VISIBLE);
                enableLocationTracking();
                validateGPSProvider(this);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.go_settings:
                navigateToSettings();
                break;
            case R.id.go_forecast:
                navigateToForecast();
                break;
            case R.id.end_session:
                showEndWorkSessionDialog();
                break;
            default:
                break;
        }
        return true;
    }

    private void showEndWorkSessionDialog() {
        DialogUtils.showChoiceDialog(this,
                getString(R.string.end_work_session_msg),
                getString(R.string.end_work_session_cancel),
                getString(R.string.end_work_session_confirm),
                null,
                () -> EventBus.getDefault().post(new EventBusHelper.EndSessionEvent(false))

        );
    }

    private void navigateToAppSettings() {
        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(i);
    }

    private void navigateToSettings() {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    private void navigateToForecast() {
        Intent i = new Intent(this, ForecastActivity.class);
        startActivity(i);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mNavListener = item -> {
        switch (item.getItemId()) {
            case R.id.action_main:
                FragmentManagingHelper.addBackStackFragment(getSupportFragmentManager(), MainFragment.newInstance());
                break;
            case R.id.action_line_up:
                FragmentManagingHelper.addBackStackFragment(getSupportFragmentManager(), StatisticsFragment.newInstance());
                break;
            case R.id.action_history:
                FragmentManagingHelper.addBackStackFragment(getSupportFragmentManager(), HistoryFragment.newInstance());
                break;
        }
        return true;
    };

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(f != null) {
            if(doubleBackToExitPressedOnce) {
                finish();
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.click_back_to_exit, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, EXIT_DELAY_MILLIS);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(EventBusHelper.GPSEvent gpsEvent) {
        if (gpsEvent != null && mGpsPermissionLayout.getVisibility() == GONE) {
            if (gpsEvent.getData().isEnabled()) {
                mGpsRequestLayout.setVisibility(GONE);
            } else {
                mGpsRequestLayout.setVisibility(VISIBLE);
            }
        }
    }
}
