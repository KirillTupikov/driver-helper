package com.driverhelp.activity;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.RadioGroup;

import com.driverhelp.R;
import com.driverhelp.ThisApplication;
import com.driverhelp.utils.EventBusHelper;
import com.driverhelp.utils.PreferenceHelper;

import org.greenrobot.eventbus.EventBus;

public class SettingsActivity extends AppCompatActivity {

    private int coeffValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new MyPreferenceFragment()).commit();
        coeffValue = PreferenceHelper.getTestCof(this);
        final RadioGroup mRadio = findViewById(R.id.radio_coeff);

        if (coeffValue == 5) {
            mRadio.check(R.id.r_1);
        } else if (coeffValue == 20) {
            mRadio.check(R.id.r_2);
        } else if (coeffValue == 50) {
            mRadio.check(R.id.r_3);
        } else if (coeffValue == 100) {
            mRadio.check(R.id.r_4);
        } else if (coeffValue == 250) {
            mRadio.check(R.id.r_5);
        } else if (coeffValue == 500) {
            mRadio.check(R.id.r_6);
        }

        mRadio.setOnCheckedChangeListener((radioGroup, i) -> {
            switch (radioGroup.getCheckedRadioButtonId()) {
                case R.id.r_0:
                    setTestCoeff(1);
                    break;

                case R.id.r_1:
                    setTestCoeff(5);
                    break;

                case R.id.r_2:
                    setTestCoeff(20);
                    break;

                case R.id.r_3:
                    setTestCoeff(50);
                    break;

                case R.id.r_4:
                    setTestCoeff(100);
                    break;

                case R.id.r_5:
                    setTestCoeff(250);
                    break;

                case R.id.r_6:
                    setTestCoeff(500);
                    break;

            }
        });
    }

    private void setTestCoeff(int testCoeff) {
        PreferenceHelper.setTestCof(this, testCoeff);
        EventBus.getDefault().post(new EventBusHelper.EndSessionEvent(false));
    }


    public static class MyPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences_activity);
        }

        @Override
        public void onPause() {
            super.onPause();

            ThisApplication.getInstance().setDayNightThemeType();
        }
    }
}
